mod ui;

use launcher_bootstrap::{DownloadResult, DownloadStatus, HashCompareResult, UpdatePackageList};
use std::path::{Path, PathBuf};
use std::process::exit;
use std::thread::sleep;
use std::time::{Duration, SystemTime, UNIX_EPOCH};

const LAUNCHER_NAME: &str = "jfx.launcher";

fn main() {
    println!("\nVersion: 4");

    let current_executable = std::env::current_exe().unwrap();
    let args: Vec<String> = std::env::args().collect();
    if args.len() >= 2 {
        // FIXME: create desktop icons
        sleep(Duration::from_millis(500));
        let exec_to_update_path = &args[1];
        let exec_to_update_path = Path::new(exec_to_update_path);

        println!("Updated file: {:?}", exec_to_update_path);
        println!("Copy file: {:?}", current_executable);

        std::fs::copy(current_executable, exec_to_update_path).unwrap();
        std::process::Command::new(exec_to_update_path)
            .spawn()
            .unwrap();
        return;
    }

    ui::start();
}

pub async fn launch<F>(message_receiver: F)
where
    F: Fn(DownloadStatus),
{
    check_and_update_updater(&message_receiver).await;
    check_and_run_launcher(&message_receiver).await;
}

pub async fn check_and_update_updater<F>(message_receiver: F)
where
    F: Fn(DownloadStatus),
{
    // FIXME: do not update if already newest
    // 1. Download updater in temp dir
    // 2. Replace updater

    println!("Loading package list");
    let list =
        UpdatePackageList::get("http://145.239.84.247:8180/updates-service/updater/manifest.json")
            .await
            .unwrap();
    println!("Searching package...");
    let info = list.find_package_info().unwrap();
    println!("Package found...");
    let data = info.load_update_package_info().await.unwrap();

    if data.files.len() != 1 {
        panic!("Broken updater manifest. It contains more than 1 entry")
    }

    let mut temp_data_dir = std::env::temp_dir();
    temp_data_dir.push(format!(
        "updater.{}",
        SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs()
    ));
    std::fs::create_dir_all(&temp_data_dir).unwrap();
    let temp_path = temp_data_dir.canonicalize().unwrap();

    let current_executable = std::env::current_exe().unwrap();
    let updater_artifact = &data.files[0];

    if updater_artifact.compare_hash(&current_executable) == HashCompareResult::HashEquals {
        message_receiver(DownloadStatus::Progress(
            updater_artifact.file.clone(),
            updater_artifact.size,
        ));
        message_receiver(DownloadStatus::Finish(updater_artifact.file.clone()));
        return;
    }

    message_receiver(DownloadStatus::TotalBytes(updater_artifact.size));
    match updater_artifact
        .download(&temp_path, message_receiver)
        .await
    {
        DownloadResult::Failed(s) | DownloadResult::NotUpdated(s) => {
            panic!("Failed to download: {:?}", s)
        }
        DownloadResult::Updated(executable_path) => {
            make_executable(&executable_path);
            println!("Executable path: {:?}", executable_path);
            std::process::Command::new(executable_path)
                .arg(current_executable)
                .spawn()
                .unwrap();
            exit(0)
        }
    }
}

pub async fn check_and_run_launcher<F>(message_receiver: F)
where
    F: Fn(DownloadStatus),
{
    let list =
        UpdatePackageList::get("http://145.239.84.247:8180/updates-service/launcher/manifest.json")
            .await
            .unwrap();
    let info = list.find_package_info().unwrap();
    let data = info.load_update_package_info().await.unwrap();
    let path = get_install_path();
    data.download(path.as_path(), message_receiver).await;

    println!("Start launcher...");
    start_launcher(&path);

    exit(0)
}

fn get_install_path() -> PathBuf {
    let mut install_root = get_install_root();
    install_root.push(LAUNCHER_NAME);
    install_root.push("launcher");
    std::fs::create_dir_all(&install_root).unwrap();
    install_root.canonicalize().unwrap()
}

#[cfg(target_family = "unix")]
fn get_install_root() -> PathBuf {
    let var = std::env::var("HOME").unwrap();
    Path::new(&var).canonicalize().unwrap()
}

#[cfg(target_os = "windows")]
fn get_install_root() -> PathBuf {
    let var = std::env::var("APPDATA").unwrap();
    Path::new(&var).canonicalize().unwrap()
}

#[cfg(target_family = "unix")]
fn make_executable(file: &Path) {
    let mut permissions = std::fs::metadata(&file).unwrap().permissions();
    std::os::unix::fs::PermissionsExt::set_mode(&mut permissions, 0o755);
    std::fs::set_permissions(&file, permissions).unwrap();
}

#[cfg(not(target_family = "unix"))]
fn make_executable(_file: &Path) {
    // no-op
}

#[cfg(target_family = "unix")]
fn start_launcher(root: &Path) {
    let mut executable_path = root.to_path_buf();
    executable_path.push("bin");
    let mut java_path = executable_path.clone();
    java_path.push("java");
    make_executable(&java_path);
    executable_path.push("launcher");
    make_executable(&executable_path);
    std::process::Command::new(executable_path).spawn().unwrap();
}

#[cfg(target_os = "windows")]
fn start_launcher(root: &PathBuf) {
    let mut executable_path = root.clone();
    executable_path.push("bin");
    executable_path.push("launcher.bat");
    std::process::Command::new(executable_path).spawn().unwrap();
}
