use launcher_bootstrap::DownloadStatus;
use orbtk::prelude::*;

pub fn start() {
    Application::new()
        .window(|ctx| {
            Window::new()
                .title("Window title")
                .size(480, 180)
                .resizeable(true)
                .child(MainView::new().build(ctx))
                .build(ctx)
        })
        .run();
}

enum Msg {
    DownloadBegin(u64),
    DownloadProgress(u64),
}

#[derive(Default, AsAny)]
struct MainState {
    total_bytes: u64,
    downloaded_bytes: u64,
    _job_thread: Option<std::thread::JoinHandle<()>>,
}

impl State for MainState {
    fn init(&mut self, _registry: &mut Registry, _ctx: &mut Context) {
        let message_adapter = _ctx.message_adapter();
        let entity = _ctx.widget().entity();
        self._job_thread = Some(std::thread::spawn(move || {
            let rt = tokio::runtime::Runtime::new().unwrap();
            rt.block_on(crate::launch(|status| match status {
                DownloadStatus::TotalBytes(total) => {
                    message_adapter.send_message(Msg::DownloadBegin(total), entity);
                }
                DownloadStatus::Start(_) => {}
                DownloadStatus::Progress(_, downloaded) => {
                    message_adapter.send_message(Msg::DownloadProgress(downloaded), entity);
                }
                DownloadStatus::Finish(_) => {}
                DownloadStatus::Error(_) => {}
            }))
        }));
    }

    fn messages(
        &mut self,
        mut _messages: MessageReader,
        _registry: &mut Registry,
        _ctx: &mut Context,
    ) {
        for msg in _messages.read::<Msg>() {
            match msg {
                Msg::DownloadBegin(total_bytes) => {
                    self.total_bytes = total_bytes;
                    self.downloaded_bytes = 0;
                }
                Msg::DownloadProgress(downloaded) => {
                    self.downloaded_bytes += downloaded;
                    let val = self.downloaded_bytes as f64 / self.total_bytes as f64;
                    MainView::val_set(&mut _ctx.widget(), val);
                    MainView::text_set(&mut _ctx.widget(), format!("{:.2}%", val * 100_f64));
                }
            }
        }
    }
}

widget!(MainView<MainState> {
    val: f64,
    text: String
});

impl Template for MainView {
    fn template(self, _id: Entity, _ctx: &mut BuildContext) -> Self {
        self.val(0.0)
            .child(
                ProgressBar::new()
                    .val(_id)
                    .v_align(Alignment::Center)
                    .build(_ctx),
            )
            .margin(15)
            .text("0.00")
            .child(
                TextBlock::new()
                    .text(_id)
                    .v_align(Alignment::Center)
                    .h_align(Alignment::Center)
                    .build(_ctx),
            )
    }
}
