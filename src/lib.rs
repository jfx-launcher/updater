use os_info::Bitness;
use serde::{Deserialize, Serialize};
use sha1::{Digest, Sha1};
use std::io::{Read, Write};
use std::path::{Path, PathBuf};

#[cfg(target_os = "linux")]
const SYSTEM: &str = "linux";
#[cfg(target_os = "windows")]
const SYSTEM: &str = "windows";
#[cfg(target_os = "macos")]
const SYSTEM: &str = "osx";

#[derive(Serialize, Deserialize, Debug)]
pub struct UpdatePackageList {
    #[serde(alias = "updatePackageType")]
    update_package_type: String,
    manifests: Vec<UpdatePackageManifest>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct UpdatePackageManifest {
    #[serde(alias = "updatePackageType")]
    update_package_type: String,
    arch: String,
    system: String,
    #[serde(alias = "enabledVersion")]
    enabled_version: String,
    updates: Vec<UpdatePackageInfo>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct UpdatePackageInfo {
    #[serde(alias = "updatePackageType")]
    update_package_type: String,
    arch: String,
    system: String,
    version: String,
    #[serde(alias = "url")]
    uri: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct UpdatePackageData {
    #[serde(alias = "updatePackageType")]
    update_package_type: String,
    arch: String,
    system: String,
    version: String,
    pub files: Vec<FileArtifact>,
    symlinks: Vec<SymlinkArtifact>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct FileArtifact {
    pub file: String,
    hash: String,
    pub size: u64,
    #[serde(alias = "url")]
    uri: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SymlinkArtifact {
    target: String,
    source: String,
}

pub enum DownloadResult {
    NotUpdated(String),
    Updated(PathBuf),
    Failed(String),
}

#[derive(PartialEq)]
pub enum HashCompareResult {
    FileNotExist,
    HashEquals,
    HashNotEquals,
}

pub enum DownloadStatus {
    TotalBytes(u64),
    Start(String),
    Progress(String, u64),
    Finish(String),
    Error(String),
}

impl UpdatePackageList {
    pub async fn get(uri: &str) -> Result<UpdatePackageList, reqwest::Error> {
        let response_result = reqwest::get(uri).await;
        match response_result {
            Ok(result) => result.json().await,
            Err(err) => Result::Err(err),
        }
    }

    pub fn find_package_info(&self) -> Option<&UpdatePackageInfo> {
        let manifest = self.find_manifest().unwrap(); // FIXME: display error?
        for package_info in &manifest.updates {
            if package_info.version == manifest.enabled_version {
                return Option::Some(package_info);
            }
        }
        Option::None
    }

    fn find_manifest(&self) -> Option<&UpdatePackageManifest> {
        let info = os_info::get();
        let bitness = info.bitness();

        let arch = if bitness == Bitness::X64 {
            "x86_64"
        } else {
            "x86"
        };

        for manifest in &self.manifests {
            if manifest.arch == arch && manifest.system == SYSTEM {
                return Option::Some(manifest);
            }
        }
        Option::None
    }
}

impl UpdatePackageInfo {
    pub async fn load_update_package_info(&self) -> Result<UpdatePackageData, reqwest::Error> {
        let response_result = reqwest::get(&self.uri).await;
        match response_result {
            Ok(result) => result.json().await,
            Err(err) => Result::Err(err),
        }
    }
}

impl UpdatePackageData {
    pub async fn download<F>(&self, root: &Path, message_receiver: F) -> Vec<DownloadResult>
    where
        F: Fn(DownloadStatus),
    {
        let mut vec = Vec::new();
        let x = self.files.iter().map(|f| f.size).sum();
        message_receiver(DownloadStatus::TotalBytes(x));
        for file in &self.files {
            vec.push(file.download(root, &message_receiver).await);
        }

        for symlink in &self.symlinks {
            symlink.download(root);
        }

        vec
    }
}

impl FileArtifact {
    pub async fn download<F>(&self, root: &Path, message_receiver: F) -> DownloadResult
    where
        F: Fn(DownloadStatus),
    {
        message_receiver(DownloadStatus::Start(self.file.clone()));
        let mut path: PathBuf = root.to_path_buf();
        path.push(&self.file);
        std::fs::create_dir_all(path.parent().unwrap()).unwrap();

        let canonical = &path.parent().unwrap().canonicalize().unwrap();

        if !&path.starts_with(canonical) {
            // FIXME: do not panic?
            panic!(
                "root breached, root: {:?}, path: {:?}, combined: {:?}",
                root, &self.file, canonical
            )
        }

        let mut response = reqwest::get(&self.uri).await.unwrap();
        if response.status() != 200 {
            // FIXME: do not panic?
            panic!("Can't read file: {}", &self.file)
        }

        if self.compare_hash(&path) == HashCompareResult::HashEquals {
            message_receiver(DownloadStatus::Progress(self.file.clone(), self.size));
            message_receiver(DownloadStatus::Finish(self.file.clone()));
            return DownloadResult::NotUpdated(self.file.clone());
        }

        let mut file = std::fs::File::create(&path).unwrap();

        let mut sha1 = Sha1::new();
        loop {
            match response.chunk().await.unwrap() {
                None => {
                    let array = sha1.finalize();
                    let downloaded_hash = format!("{:x}", array);
                    if !downloaded_hash.eq_ignore_ascii_case(&self.hash) {
                        // FIXME: do not panic
                        // panic!("Failed to download a file. Hash error: File: {:?}", &self.file)
                        println!("Failed to update: {}", &self.file);
                        message_receiver(DownloadStatus::Error(self.file.clone()));
                        return DownloadResult::Failed(self.file.clone());
                    }
                    // FIXME: extract somewhere to communicate with the GUI
                    println!("File updated: {}", &self.file);
                    message_receiver(DownloadStatus::Finish(self.file.clone()));
                    return DownloadResult::Updated(path);
                }
                Some(x) => {
                    let i = x.len();
                    sha1.update(&x);
                    file.write_all(&x).unwrap();
                    // total_read += i as u64;
                    message_receiver(DownloadStatus::Progress(self.file.clone(), i as u64))
                }
            }
        }
    }

    pub fn compare_hash(&self, file_path: &Path) -> HashCompareResult {
        if !file_path.exists() {
            return HashCompareResult::FileNotExist;
        }

        let mut sha1 = Sha1::new();
        let mut file = std::fs::File::open(file_path).unwrap();
        let mut buf: [u8; 8 * 1024] = [0; 8 * 1024];

        loop {
            let r = file.read(&mut buf).unwrap();
            if r == 0 {
                break;
            }
            sha1.update(&buf[..r]);
        }

        let array = sha1.finalize();
        let downloaded_hash = format!("{:x}", array);
        return if downloaded_hash.eq_ignore_ascii_case(&self.hash) {
            println!("Good hash, file: {:?}", &self.file);
            HashCompareResult::HashEquals
        } else {
            println!(
                "Bad hash, file: {:?}, \n Local Hash: {:?}\nRemote Hash: {:?}",
                &self.file, downloaded_hash, &self.hash
            );
            HashCompareResult::HashNotEquals
        };
    }
}

impl SymlinkArtifact {
    #[cfg(target_family = "unix")]
    pub fn download(&self, root: &Path) {
        let mut path: PathBuf = root.to_path_buf();
        path.push(&self.target);
        path = std::fs::canonicalize(path).unwrap();
        if !&path.starts_with(root) {
            // FIXME: do not panic?
            panic!(
                "root breached, root: {:?}, target: {:?}, combined: {:?}",
                root, &self.target, &path
            )
        }

        let mut link: PathBuf = root.to_path_buf();
        link.push(&self.source);
        if !&link.starts_with(root) {
            // FIXME: do not panic?
            panic!(
                "root breached, root: {:?}, source: {:?}, combined: {:?}",
                root, &self.source, &link
            )
        }

        std::os::unix::fs::symlink(path, link).unwrap();
    }

    #[cfg(not(target_family = "unix"))]
    pub fn download(&self, root: &Path) {
        // no-op
    }
}
